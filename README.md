# OpenML dataset: Bechdel-Cast-Data

https://www.openml.org/d/43499

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
"In the Bechdel Cast, the question asked, do movies have women in them? Are all their discussions involve boyfriends or husbands or do they have individualism? The Patriarchy's vast. Let's start changing it with the Bechdel Cast."  
This dataset contains the Episode information for the comedy podcast, "The Bechdel Cast", it is a feminist podcast that examines movies through a feminist lens. One of the ways the co-hosts examine movies is by seeing if it passes the Bechdel Test. They also rate each movie at the end with a "Nipple" scale to judge how well the movie represents women. 
Content
The dataset contains pertinent information such as the movie name, the weekly guest's name, the date it aired, the genre of the movie, whether it passed the Bechdel test, and the nipple rating for each host and guest, as well as the average rating for the movie that they discussed that episode. 
Acknowledgements
Thank you, Caitlin Durante and Jamie Loftus for hosting each week, and Wikipedia for having the episode information.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43499) of an [OpenML dataset](https://www.openml.org/d/43499). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43499/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43499/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43499/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

